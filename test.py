import pylab as pb
import numpy as np
import math
import random
import matplotlib.pyplot as plt
#import matplotlib.mlab as mlab

from scipy.spatial.distance import cdist
from scipy.stats import multivariate_normal

from numpy.linalg import inv
from numpy.linalg import det
from numpy import matmul
from numpy import trace

%matplotlib inline

#%%
#Question 12, generating data
N = 1000
x = np.arange(-1, 1, 0.01)
y = np.arange(-1, 1, 0.01)
for t in range(200):
    y[t] = -1.3*x[t] + 0.5 + np.random.normal(0, 0.3)
plt.scatter(x, y)

#%%
#Q12
#Generating sn and mn
def calcMeanCovariance(alpha, beta, phi, y):
    cov = inv(alpha*np.identity(2) + beta*matmul(phi.T, phi))
    mu = matmul(cov, beta*matmul(phi.T, y))
    return mu.reshape(2,), cov

phi = np.zeros((201,2))
x =  np.zeros((201,1))
y =  np.zeros((201,1))

phi[:,0].fill(1)
noiseCov = 0.3
alpha = 2
beta = 1/noiseCov

for i in range(201):
    x[i] = -1 + i*0.01
    phi[i,1] = x[i]
    y[i] = -1.3*x[i]+0.5+np.random.normal(0, noiseCov)

#TODO - randomise order of y, phi

means = {}
covs = {}
for i in np.arange(0, 201):
    i =int(i)
    m, s = calcMeanCovariance(alpha, beta, phi[0:i], y[0:i])
    means[i] = m
    covs[i] = s

#%%
#Sampling from posterior, using pointsUsed points
samples = 10
pointsUsed = 10
f = np.zeros((samples, 2))
w = np.linspace(-1.5, 1.5, N)

plt.ylabel('y')
plt.xlabel('x')
plt.title(str(samples) + ' samples, using ' + str(pointsUsed) + ' data points')
plt.scatter(x[0:pointsUsed],y[0:pointsUsed])

for i in range(f[:, 0].shape[0]):
    f[i] =  np.random.multivariate_normal(means[pointsUsed], covs[pointsUsed])
    plt.plot(w, f[i][1] * w + f[i][0])

plt.savefig('Q12_' + str(pointsUsed) + '_samples.png')


#%%
#legend code https://github.com/matplotlib/matplotlib/issues/11134
xc = np.linspace(-4, 4, N)
yc = np.linspace(-4, 4, N)
x1p, x2p = np.meshgrid(xc, yc)
pos = np.vstack((x1p.flatten(), x2p.flatten())).T

pdf0 = multivariate_normal(means[0], covs[0])
Z0 = pdf0.pdf(pos).reshape(N, N)

pdf1 = multivariate_normal(means[1], covs[1])
Z1 = pdf1.pdf(pos).reshape(N, N)

pdf10 = multivariate_normal(means[10], covs[10])
Z10 = pdf10.pdf(pos).reshape(N, N)

pdf50 = multivariate_normal(means[50], covs[50])
Z50 = pdf50.pdf(pos).reshape(N, N)

pdf200 = multivariate_normal(means[200], covs[200])
Z200 = pdf200.pdf(pos).reshape(N, N)

level = np.logspace(1.01, 4, 20) / 1000

#plt.title("Posterior comparison")
#plt.contour(x1p, x2p, Z1, levels=level)
#plt.xlabel('w0')
#plt.ylabel('w1')

fig, ax = plt.subplots(figsize=(8, 8))
ax.set_title('Posterior comparison')
ax.set_xlabel('w0')
ax.set_ylabel('w1')
c0   = ax.contour(x1p, x2p, Z0,   levels=level, colors='black')
c1   = ax.contour(x1p, x2p, Z1,   levels=level, colors='red')
c10  = ax.contour(x1p, x2p, Z10,  levels=level, colors='yellow')
c50  = ax.contour(x1p, x2p, Z50,  levels=level, colors='blue')
c200 = ax.contour(x1p, x2p, Z200, levels=level, colors='green')

h0, _ = c0.legend_elements()
h1, _ = c1.legend_elements()
h10, _ = c10.legend_elements()
h50, _ = c50.legend_elements()
h200, _ = c200.legend_elements()
ax.legend([h0[0], h1[0], h10[0], h50[0], h200[0]], ['0', '1', '10', '50', '200'])

fig.savefig('Posterior comparison.png')

#plt.clabel(CS, inline=1, fontsize = 8)
#plt.savefig('Q12_1point_posterior.png')
# %%
#Guassian process prior - Q13
x = np.linspace(-5, 5, 210)
x = x.reshape(-1, 1)
mu= np.zeros(x.shape)

fig, ax = plt.subplots(5, sharex=True, sharey=True, figsize=(7,10))

for i in range(5):
    l = i + 0.01
    K = np.exp(-cdist(x, x)*(1/(l*l)))
    f = np.random.multivariate_normal(mu.flatten(), K, 4)
    ax[i].plot(x, f.T)
    ax[i].text(-5, -3, 'l = ' + str(l), fontsize=18)
fig.subplots_adjust(hspace = 0)
fig.savefig('q13.png')

#%%
x = np.linspace(-5, 5, 200).reshape(-1, 1)
dataSamples = 7
xt = np.linspace(-math.pi, math.pi, dataSamples).reshape(-1, 1)
yt = np.zeros(xt.shape)
for i in range(dataSamples):
    yt[i] = math.sin(xt[i]) + np.random.normal(0, 0.5)

#%%
#Q14
def kernel(x, y):
    l = 8
    p = 1
    #d = cdist(x, y)
    #sin = math.sin((math.pi / p) * abs(d))
    #return np.exp(-2 * math.sin((math.pi * abs(d).T.dot(x - y))) /p)/(l*l))
    #return np.exp(-2 * sin/(l*l))
    return np.exp(-cdist(x, y)*(1/(l*l)))

K = kernel(xt, xt)
K = K + 0.01*np.identity(K.shape[0])
Ks = kernel(xt, x)
Kss = kernel(x, x)

mu = matmul(matmul(Ks.T, inv(K)), yt)
var = Kss - matmul(matmul(Ks.T, inv(K)), Ks)

fPosterior = np.random.multivariate_normal(mu.flatten(), var, 5)
plt.plot(x, fPosterior.T)
plt.scatter(xt, yt, c='black', s= 100)
#muM = mu.reshape((200,)) - np.sqrt(var)
#print(muM.shape)
#plt.gca().fill_between(x.flatten(), muM, mu+var)
#plt.contour(x1p, x2p, )
#plt.savefig('Q14_noise.png')

print(mu.shape)
print(var.shape)
#x1p, x2p = np.meshgrid(x, x)
#pos = np.vstack((x1p.flatten(), x2p.flatten())).T

#pdf = multivariate_normal(mu.flatten(), var)
#Z = pdf.pdf(pos)
#print(Z.shape)

#%%
#Question 21

import scipy as sp
import scipy.optimize as opt
def f_nonlin(xi):
    return (xi*math.sin(xi), xi*math.cos(xi))

absX = 100
x = np.linspace(0, 4*math.pi, absX).reshape(-1, 1)

fnonlin = np.zeros((2, 100))
for i in range(100):
    #fnonlin[0, i], fnonlin[1, i] = f_nonlin(x[i])
    fnonlin[:, i] = f_nonlin(x[i])

#plt.scatter(fnonlin[0, :], fnonlin[1, :])
A = np.random.normal(0, 1, (10, 2))
x0 = np.random.normal(0, 1, (10, 2))

Y = matmul(A, fnonlin)

def C(W):
    sigma = 1
    return matmul(W, W.T) + (sigma**2) * np.identity(W.shape[0])

def f(x, *args):
    x = x.reshape((10, 2))
    const = 0
    return const + math.log(det(C(x))) + np.trace(matmul(matmul(Y.T, inv(C(x))), Y))

def dfx(x, *args):
    x = x.reshape((10, 2))
    c = C(x)
    m = np.zeros((10, 2))
    for i in range(10):
        for j in range(2):
            Jij = np.zeros((2, 10))
            Jji = np.zeros((10, 2))

            Jij[j, i] = 1
            Jji[i, j] = 1

            dC = matmul(x, Jij) + matmul(Jji, x.T)
            dL = trace(matmul(inv(c), dC)) + trace(matmul(matmul(Y, Y.T), matmul(matmul(-inv(c), dC), inv(c))))
            m[i, j] = dL
    return m.flatten()

x_star = opt.fmin_cg(f, x0.flatten(), fprime=dfx).reshape((10, 2))

n = matmul(np.linalg.pinv(x_star), Y)

fig = plt.figure(figsize=(6, 8))
ax1 = plt.subplot(211)
ax2 = plt.subplot(212, projection='polar')
ax1.scatter(n[0], n[1])
ax2.scatter(n[0], n[1])

plt.show()
